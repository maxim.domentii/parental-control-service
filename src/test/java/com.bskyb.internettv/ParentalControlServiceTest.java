package com.bskyb.internettv;

import com.bskyb.internettv.parental_control_service.ParentalControlService;
import com.bskyb.internettv.parental_control_service.impl.ParentalControlServiceImpl;
import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.thirdparty.TitleNotFoundException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ParentalControlServiceTest {

    private MovieService movieService;
    private ParentalControlService parentalControlService;

    @Before
    public void setUp(){
        movieService = mock(MovieService.class);
        parentalControlService = new ParentalControlServiceImpl(movieService);
    }

    @Test(expected = TitleNotFoundException.class)
    public void testCanWatchMovie_whenMovieServiceThrowTitleNotFoundException() throws Exception {
        String customerParentalControlLevel = "U";
        String movieId = "1";

        when(movieService.getParentalControlLevel(movieId)).thenThrow(new TitleNotFoundException());

        parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
    }

    @Test
    public void testCanWatchMovie_whenMovieServiceThrowTechnicalFailureException() throws Exception {
        String customerParentalControlLevel = "U";
        String movieId = "1";

        when(movieService.getParentalControlLevel(movieId)).thenThrow(new TechnicalFailureException());

        boolean result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);
    }

    @Test
    public void testCanWatchMovie_whenMovieServiceReturnsHigherLevelThanCustomerLevel() throws Exception {
        String movieId = "1";

        String customerParentalControlLevel = "U";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("PG");
        boolean result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("12");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("15");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("18");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        //==================================

        customerParentalControlLevel = "PG";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("12");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("15");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("18");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        //==================================

        customerParentalControlLevel = "12";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("15");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("18");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);

        //==================================

        customerParentalControlLevel = "15";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("18");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(false, result);
    }

    @Test
    public void testCanWatchMovie_whenMovieServiceReturnsLowerOrEqualLevelThanCustomerLevel() throws Exception {
        String movieId = "1";

        String customerParentalControlLevel = "18";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("U");
        boolean result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("PG");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("12");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("15");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("18");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        //==================================

        customerParentalControlLevel = "15";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("U");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("PG");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("12");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("15");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        //==================================

        customerParentalControlLevel = "12";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("U");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("PG");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("12");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        //==================================

        customerParentalControlLevel = "PG";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("U");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        when(movieService.getParentalControlLevel(movieId)).thenReturn("PG");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

        //==================================

        customerParentalControlLevel = "U";

        when(movieService.getParentalControlLevel(movieId)).thenReturn("U");
        result = parentalControlService.canWatchMovie(customerParentalControlLevel, movieId);
        assertEquals(true, result);

    }
}
