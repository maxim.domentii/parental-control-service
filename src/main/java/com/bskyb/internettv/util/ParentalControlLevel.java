package com.bskyb.internettv.util;

import java.security.InvalidParameterException;

public enum ParentalControlLevel {
    U(1, "U"),
    PG(2, "PG"),
    _12(3, "12"),
    _15(4, "15"),
    _18(5, "18");

    private int ordinal;
    private String level;

    ParentalControlLevel(int ordinal, String level){
        this.level = level;
        this.ordinal = ordinal;
    }

    public static ParentalControlLevel getParentalControlLevelByStringLevel(String level){
        for (ParentalControlLevel parentalControlLevel : ParentalControlLevel.values()){
            if (parentalControlLevel.getLevel().equals(level)){
                return parentalControlLevel;
            }
        }

        throw new InvalidParameterException("Given parental control level is not valid");
    }

    public int getOrdinal() {
        return ordinal;
    }

    public String getLevel() {
        return level;
    }
}
