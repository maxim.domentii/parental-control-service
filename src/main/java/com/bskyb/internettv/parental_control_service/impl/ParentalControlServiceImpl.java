package com.bskyb.internettv.parental_control_service.impl;

import com.bskyb.internettv.parental_control_service.ParentalControlService;
import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.util.ParentalControlLevel;

public class ParentalControlServiceImpl implements ParentalControlService {

    private MovieService movieService;

    public ParentalControlServiceImpl(MovieService movieService){
        this.movieService = movieService;
    }

    public boolean canWatchMovie(String customerParentalControlLevel, String movieId) throws Exception {
        String movieParentalControlLevel;
        try {
            movieParentalControlLevel = movieService.getParentalControlLevel(movieId);
        } catch (TechnicalFailureException ex) {
            return false;
        }

        ParentalControlLevel customerLevel =
                ParentalControlLevel.getParentalControlLevelByStringLevel(customerParentalControlLevel);
        ParentalControlLevel movieLevel =
                ParentalControlLevel.getParentalControlLevelByStringLevel(movieParentalControlLevel);

        return movieLevel.getOrdinal() <= customerLevel.getOrdinal();
    }
}
